# Expressions

there is no return keyword, the last expressions is always returned

## Let

```
// Types can be inferred
let name: Type = Expr
// x is a F32
let x = 1.0_F32
```

```
// let expressions return the variables values
so let x = let y = 10
x = 10 and y = 10
```

### Destructuring

```
// let is a match experssions so...
let {x: x, y: y}: Vec2::<ISize> = Vec2{ x: 1, y: 5 }
let Vec2{x: x, y: y} = Vec2{ x: 1, y: 5 }
let {x: x, ...rest } = { x: 1, y: 5, z: -2 }

let (x, y, z): (String, ISize, FSize) = ("Hello", -10, 2.0)
let (x, y, z) = ("Hello", -10, 2.0)
let (x, ...rest) = ("Hello", -10, 2.0)

let [x, y, z]: [String, ISize, FSize] = ["Hello", -10, 2.0]
let [x, y, z] ["Hello", -10, 2.0]
let [x, ...rest] ["Hello", -10, 2.0]
```

## match

match is an expression

```
match Expr { MatchCase -> Expr ... }

match x {
    0 -> os::io::println("Zero")
    1 -> os::io::println("One")
    // catch all
    _ -> os::io::println("A Number")
}

// where person is the struct Person { age: USize, name: String }
match person {
    Person{ name: "Bob" } -> os::io::println("Hello, Bob!")
    { age: person_age, name: person_name } -> {
        os::io::println("I don't know you {}, you are {} years old", person_name, person_age)
    }
}
```

## if

if is an expression, else is required

```
if Expr { Expr } else { Expr }
if Expr { Expr } else if Expr { Expr } else if { Expr } else { Expr }

if variable {
    os::io::println("if branch")
} else {
    os::io::println("else branch")
}

if variable {
    os::io::println("if branch")
} else if other_variable {
    os::io::println("if else branch")
} else {
    os::io::println("else branch")
}
```

Next [Functions](spec/functions.md)
