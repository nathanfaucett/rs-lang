# rs-lang

a language

- [Expressions](spec/expressions.md)
- [Functions](spec/functions.md)
- [Structs](spec/structs.md)
- [Modules](spec/modules.md)
- [Traits](spec/traits.md)
